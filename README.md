# @shootismoke/cities

Up-to-date air quality data for 100+ cities, used to populate data on static pages.

## Available Commands

The repository contains 3 commands:

-   `yarn ts-node src/addCities.ts`: reads from the `worldcities.csv` file, and, for each city, creates a JSON file in the `cities/` folder populated with basic data (name, lat/lng, country...).
-   `yarn ts-node src/populateCities.ts`: reads files inside the `cities/` folder, and for each city, populates its AQI data from WAQI and OpenAQ inside the file.
-   `yarn ts-node src/generateAll.ts`: merges all JSONs inside `cities/` into one huge JSON file called `all.json`.
