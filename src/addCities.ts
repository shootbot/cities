// Sh**t! I Smoke
// Copyright (C) 2018-2020  Marcelo S. Coelho, Amaury M.

// Sh**t! I Smoke is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Sh**t! I Smoke is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with Sh**t! I Smoke.  If not, see <http://www.gnu.org/licenses/>.

import slugify from '@sindresorhus/slugify';
import axios from 'axios';
import csvtojson from 'csvtojson';
import fs from 'fs';

/**
 * A city as represented inside `../worldcities.csv`.
 */
interface CsvCity {
	admin_name: string;
	city: string;
	city_ascii: string;
	country: string;
	lat: number;
	lng: number;
}

/**
 * Generate city files for this amount of cities.
 */
const NUMBER_OF_CITIES = +(process.env.NUMBER_OF_CITIES || '1000');

interface FlickrPhotoData {
	id: string;
	secret: string;
	server: string;
	farm: string;
}

interface FlickrPhotoResponse {
	photos: {
		photo?: FlickrPhotoData[];
	};
}

const FLICKR_API_KEY = process.env.FLICKR_API_KEY || 'YOUR_OWN_KEY';

/**
 * Flickr photo search endpoint.
 *
 * @see https://www.flickr.com/services/api/flickr.photos.search.html
 */
const FLICKR_API_URL = `https://api.flickr.com/services/rest?method=flickr.photos.search&format=json&api_key=${FLICKR_API_KEY}&content_type=1&per_page=1&geo_context=2&sort=relevance&privacy_filter=1`;

async function searchFlickrCity(
	text: string,
	bbox?: string
): Promise<FlickrPhotoData | undefined> {
	let url = `${FLICKR_API_URL}&text=${text}`;
	if (bbox) {
		url += `&bbox=${bbox}`;
	}

	const photoResults = (await axios
		.get<string>(encodeURI(url))
		// The response is wrapped around `jsonFlickrApi()`
		.then(({ data }) =>
			data.substring('jsonFlickrApi('.length).slice(0, -1)
		)
		.then(JSON.parse)) as FlickrPhotoResponse;

	return photoResults?.photos?.photo && photoResults.photos.photo[0];
}

/**
 * From data inside our `worldcities.csv`, generate individual JSON files with
 * city data.
 */
async function main(): Promise<void> {
	const rawCities = (await csvtojson().fromFile(
		'./worldcities.csv'
	)) as CsvCity[];
	const cities = rawCities.slice(0, NUMBER_OF_CITIES);

	// Holds a lookup of slugs we added.
	const slugsLookup: Record<string, true> = {};

	for (let i = 0; i < cities.length; i++) {
		const city = cities[i];
		console.log(
			`Processing city ${i + 1}/${cities.length}: ${city.city_ascii}...`
		);

		// Generate the slug for the current city.
		let slug: string;
		slug = slugify(city.city_ascii);
		// If we already created this slug, then the next city will have a slug
		// with the country appended.
		//
		// For example, we have 2 cities Barcelona, one in Spain, the other in
		// Venezuela. The first city will have slug `barcelona`, the second one
		// `barcelona-venezuela`.
		if (slugsLookup[slug]) {
			slug = slugify(`${city.city_ascii}-${city.country}`);
		}
		slugsLookup[slug] = true;

		// See if we already have a file for the current city.
		let rawJson: string | undefined;
		try {
			rawJson = fs.readFileSync(`./cities/${slug}.json`, 'utf8');
		} catch {
			console.log(`Creating ${slug}.json...`);
		}

		let photoData = await searchFlickrCity(
			`${city.city_ascii} city skyline`,
			`${city.lng - 1},${city.lat - 1},${city.lng + 1},${city.lat + 1}`
		);
		// If we don't have any results, search again without bbox.
		if (!photoData) {
			photoData = await searchFlickrCity(city.city_ascii);
		}
		// Use Flickr's static CDN.
		// https://www.flickr.com/services/api/misc.urls.html
		const photoUrl = photoData
			? `https://live.staticflickr.com/${photoData.server}/${photoData.id}_${photoData.secret}_m.jpg`
			: undefined;

		fs.writeFileSync(
			`./cities/${slug}.json`,
			JSON.stringify(
				{
					...(rawJson && JSON.parse(rawJson)),
					adminName: city.admin_name,
					country: city.country,
					name: city.city,
					gps: {
						latitude: +city.lat,
						longitude: +city.lng,
					},
					photoUrl,
					slug,
				},
				null,
				'\t'
			)
		);
	}
}

main().catch((err) => {
	console.error(err);
	process.exit(1);
});
