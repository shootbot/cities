// Sh**t! I Smoke
// Copyright (C) 2018-2020  Marcelo S. Coelho, Amaury M.

// Sh**t! I Smoke is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Sh**t! I Smoke is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with Sh**t! I Smoke.  If not, see <http://www.gnu.org/licenses/>.

import fs from 'fs';

import { City } from './populateCities';

/**
 * From all individual city jsons in the `/cities` folder, generate one huge
 * json file containing all data.
 */
function main(): void {
	// Read files in `/cities` folder.
	const files = fs.readdirSync('./cities');

	const allCities = files.map(
		(file) =>
			JSON.parse(fs.readFileSync(`./cities/${file}`, 'utf-8')) as City
	);

	// Sort by most cigarettes to least cigarettes.
	allCities.sort((a, b) => {
		if (a.api === undefined && b.api === undefined) {
			return 0;
		} else if (a.api === undefined) {
			return 1;
		} else if (b.api === undefined) {
			return -1;
		} else {
			return (
				b.api.shootismoke.dailyCigarettes -
				a.api.shootismoke.dailyCigarettes
			);
		}
	});

	fs.writeFileSync(`./all.json`, JSON.stringify(allCities, null, '\t'));
}

main();
