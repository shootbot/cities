// Sh**t! I Smoke
// Copyright (C) 2018-2020  Marcelo S. Coelho, Amaury M.

// Sh**t! I Smoke is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Sh**t! I Smoke is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with Sh**t! I Smoke.  If not, see <http://www.gnu.org/licenses/>.

import type { Api } from '@shootismoke/ui';
import { raceApiPromise } from '@shootismoke/ui/lib/util/api';
import { queue } from 'async';
import { debug } from 'debug';
import fs from 'fs';
import pRetry from 'p-retry';

const l = debug('shootismoke:populateCities');

/**
 * This is the JSON format that we save into each individual city json file.
 */
export interface City {
	api?: Api;
	gps: {
		latitude: number;
		longitude: number;
	};
	country: string;
	name: string;
	slug: string;
}

/**
 * Populate one city.
 */
function populateCity(city: City): Promise<Api> {
	return Promise.race<Api>([
		pRetry(
			() =>
				raceApiPromise(city.gps, {
					aqicn: {
						token: process.env.AQICN_TOKEN as string,
					},
				}),
			{ retries: 2 }
		),
		// We abort after 10s.
		new Promise<Api>((_resolve, reject) =>
			setTimeout(() => reject(new Error('10s timeout passed')), 10000)
		),
	]);
}

/**
 * Generate city files for this amount of cities.
 */
const NUMBER_OF_CITIES = +(process.env.NUMBER_OF_CITIES || '1000');

async function processFile(file: string): Promise<boolean> {
	let successful = false;
	const city = JSON.parse(
		fs.readFileSync(`./cities/${file}`, 'utf-8')
	) as City;

	let api: Api | undefined = undefined;

	try {
		const result = await populateCity(city);

		// Skip if the stations are too far.
		if (!result.shootismoke.isAccurate) {
			throw new Error(`Reverting ${file}: stations are too far.`);
		}

		api = result;
		successful = true;
	} catch (err) {
		l(`Reverting ${file}: ${(err as Error).message}`);
	}

	fs.writeFileSync(
		`./cities/${city.slug}.json`,
		JSON.stringify(
			{
				...city,
				api,
			},
			null,
			'\t'
		)
	);

	return successful;
}

/**
 * For each city, fetch its latest air quality data.
 */
async function main(): Promise<void> {
	// Read files in `/cities` folder, sorted by last edited.
	const filesWithTime = fs
		.readdirSync('./cities')
		.slice(0, NUMBER_OF_CITIES)
		.map((file) => ({
			name: file,
			time: fs.statSync(`./cities/${file}`).mtime.getTime(),
		}));
	filesWithTime.sort((a, b) => b.time - a.time);
	const files = filesWithTime.map(({ name }) => name);

	// Track the number of processed files.
	let processed = 0;
	// Track the number of successful updates.
	let successful = 0;

	// Create a queue on concurrency 20.
	const q = queue<string>((task, callback) => {
		++processed;
		l(`Processing ${processed}/${files.length}: ${task}...`);

		processFile(task)
			.then(() => {
				++successful;
				callback();
			})
			.catch(callback);
	}, 20);

	// Push all files to queue.
	await q.push(files);

	await q.drain();
	l(`Successfully updated ${successful}/${files.length} cities.`);
}

main().catch((err) => {
	console.error(err);
	process.exit(1);
});
